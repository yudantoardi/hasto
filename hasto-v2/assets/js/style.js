$(document).ready(function(){
    $(".dropdown").hover(function(){
        var dropdownMenu = $(this).children(".dropdown-menu");
        if(dropdownMenu.is(":visible")){
            dropdownMenu.parent().toggleClass("open");
        }
    });

    $(".dropright").hover(function(){
        var dropdownMenu1 = $(this).children(".dropdown-menu");
        if(dropdownMenu1.is(":visible")){
            dropdownMenu1.parent().toggleClass("open");
        }
    });

    $('.dropdown-toggle').click(function () {
        window.location = $(this).attr('href');
    });

    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });


    $(".progress-barx").ProgressBar();

    //burger
    $(".burger").click(function(){
        $(this).toggleClass("active");
        $(".navbar-nav").toggleClass("show");
    });

    if($(window).innerWidth() <= 1024){
        $(".dropdown, .dropdown-menu .nav-item").append("<span class='drop-bt'>&nbsp;</span>");
    }else{
        $(".dropdown .drop-bt").remove();
    }
    /*$(window).resize(function(){
        if($(window).innerWidth() <= 1024){

        }
    });*/
    


}); 

